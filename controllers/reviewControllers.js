const Review = require('./../models/Review');
const { getName } = require('./userControllers');
const { getProduct } = require('./productControllers');
const { getUserOrders } = require('./orderControllers');

//add a review
module.exports.addReview = async (productId, userId, reqBody) => {
    try{
        
        //get requirements
        let custName = await getName(userId);
        let product = await getProduct(productId);
        let orderList = await getUserOrders(userId);

        //check if user has bought product for review
        let haveBought = false;
        if (orderList !== null){
           
            for( eachOrder of orderList ){
                
                for ( order of eachOrder.orderItems){
                    if(order.productId == productId){
                        haveBought = true; 
                        break;
                    }
                }
                if (haveBought==true){
                    break;
                }
            }
        }

        //allow add only if user bought product
        if (haveBought == true){
            let newReview = new Review({
                productId: productId,
                productName: product.name,
                reviewerId: userId,
                reviewerName: custName.fullName,
                comments: reqBody.comments,
                rating: reqBody.rating
            })
            return newReview.save().then(result => result ? true : false);
        } else {
            return {message: `Cannot add review if product has not been bought.`}
        }

    }catch(err){
        return err;
    }
}

//retrieve list of user reviews
module.exports.getUserReviews = async (userId) => {
    try{
        return await Review.find({reviewerId: userId}).then(result => {
            if (result.length > 0){
                return result;
            } else {
                return {message: `No reviews for this user yet.`};
            }
        })
    } catch(err) {
        return err
    }
}

//retrieve all reviews
module.exports.getAllReviews = async () => {
    try{
        return await Review.find().then(result => result);
    }catch(err){
        return err;
    }
}

//retrieve single review
module.exports.getReview = async (reviewId) => {
    try{
        return await Review.findById(reviewId).then(result => {
            if (result !== null){
                return result;
            } else {
                return {message: `Review not found.`};
            }
        })
    }catch(err) {
        return err;
    }
}

//edit review: non-author cannot edit
module.exports.editReview = async (reviewId, userId, reqBody) => {

    try{
        
        return await Review.findById(reviewId).then(result => {
            //console.log(result);
            if (result.reviewerId == userId){
                return Review.findByIdAndUpdate(reviewId, {$set: {comments: reqBody.comments, rating: reqBody.rating}}).then(result => result ? true: false);
            } else {
                return {message: `Not allowed to update.`};
            }
        })

    }catch(err){
        console.log(err);
        return err;
    }
}

//delete review: non-author cannot edit
module.exports.deleteReview = async (reviewId, userId) => {
    try{
        return await Review.findById(reviewId).then(result => {
            //console.log(result);
            if (result.reviewerId == userId){
                return Review.findByIdAndDelete(reviewId).then(result => result ? true: false);
            } else {
                return {message: `Not allowed to delete.`};
            }
        })
    }catch(err){
        console.log(err);
        return err;
    }
}

//search review based on reviewer name or product name
module.exports.searchReview = async (keyword) => {
    try{
        
        let search = 
        {
            $or: [
                { productName: {$regex: keyword, $options: "i"} },
                { reviewerName: {$regex: keyword, $options: "i"} }
            ]
        }

        return await Review.find(search).then(result => result);
    }catch(err){
        console.log(err);
        return err;
    }
}