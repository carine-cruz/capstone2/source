const Order = require('./../models/Order');
const { getName
//    , addOrder 
} = require('./userControllers');
const { decreaseStocks } = require('./productControllers');
const nodemailer = require('nodemailer');

module.exports.createOrder = async (reqBody, userId) => {
    try{

        let custName = await getName(userId);

        let newOrder = new Order({
            totalAmount: reqBody.totalAmount,
            orderItems: reqBody.orderItems,
            orderedBy: custName.fullName,
            orderedById: userId
        })

        //save new order
        return await newOrder.save().then(saveResult => {

            if (saveResult){
                //add order to user array for order list. Assign to variable -- remove order array from user
                //let addResult = addOrder(userId, saveResult._id).then(result => result ? true : false);
                let decResult;
                
                for(let order of saveResult.orderItems){
                  
                    decResult = decreaseStocks(order.productId, order.productQty).then(result => result ? true : false);
            
                    if (decResult == false){
                      
                        return false;
                    } 
                }
                return saveResult;

            } else {
                return false
            }
        });

    }catch(err){
        console.log(err);
        throw err;
    }
}

module.exports.getUserOrders = async (userId) => {
    
    try{
        return await Order.find({orderedById: userId}).then(result=>{
            
            if ( result == null ){
                return {message: `No orders for this user.`};
            } else {
                return result;
            }
    });
    }catch(err){
        return err
    }
}

module.exports.getAllOrders = async () => {
    try{
        return await Order.find().then(result=>{
        
            if ( result == null ){
                return {message: `No orders yet.`};
            } else {
                return result;
            }
    });
    }catch(err){
        return err
    }
}

module.exports.getPendingOrders = async () => {
    try{
        return await Order.find({orderStatus: "On-going"}).then(result=>{
        
            if ( result == null ){
                return {message: `No pending orders.`};
            } else {
                return result;
            }
    });
    }catch(err){
        return err
    }
}

module.exports.updateOrderStatus = async (orderId, orderStatus) => {
    try{
        
        let checkPymt = await Order.findById(orderId).then(result=>{
            if (result !== null){
                return result
            } else {
                return {message: `Order does not exist.`}
            }
        })
        
        if (checkPymt.paymentStatus !== "Paid" && orderStatus.toLowerCase() == "complete"){
            return {message: "Cannot complete unpaid order."};
        } else {

            let updateBody = { orderStatus: orderStatus };

            if (orderStatus.toLowerCase() == "complete"){
                updateBody.orderFulfilled = new Date();
            }

            return Order.findByIdAndUpdate(orderId, {$set: updateBody}).then((updateResult, error)=>{

                if (updateResult !== null){
                    return true
                }else{
                    return error
                }
            })
        }
 
    }catch(err){
        return err;
    }
}

module.exports.updatePaymentStatus = async (orderId, pStatus, pId, refNo) => {
    try{

        let pymtDetails;

        if (pStatus.toLowerCase() === `paid` ){
            pymtDetails = {
                paymentStatus: pStatus,
                paymentId: pId,
                paymentDate: new Date(),
                paymentRefNo: refNo
            }
        } else {
            pymtDetails = {
                paymentStatus: pStatus,
                paymentId: pId
            }
        }

        return await Order.findByIdAndUpdate(orderId, {$set: pymtDetails}).then((updateResult, error)=>{
    
            if (updateResult !== null){
                return true
            }else{
                return error
            }
        });
    }catch(err){
        console.log(err)
        throw err;
    }
}

module.exports.getOrder = async (orderId) => {
    try{
        return await Order.findById(orderId).then(result=>{
            if ( result == null ){
                return {message: `Order not existing.`};
            } else {
                return result;
            }
        })
    }catch(err){
        console.log(err);
        throw err;
    }
}

module.exports.searchOrder = async (keyword) => {
    try{
        
        let search = 
        {
            $or: [
                { orderedBy: {$regex: keyword, $options: "i"} },
                //{ orderItems: {$regex: keyword, $options: "i"} }
                {orderItems: {$elemMatch:{productName: {$regex: keyword, $options: "i"}}}}
            ]
        }

        return await Order.find(search).then(result => result);
    }catch(err){
        console.log(err);
        return err;
    }
}

//user search using keyword for their own order
module.exports.searchUserOrder = async (keyword, userId) => {
    try{
        let search = 
        {
            $and: [
                {orderedById: userId},
                {
                    $or: [
                        { orderedBy: {$regex: keyword, $options: "i"} },
                        //{ orderItems: {$regex: keyword, $options: "i"} }
                        {orderItems: {$elemMatch:{productName: {$regex: keyword, $options: "i"}}}}
                    ]
                }
            ]
        }

        return await Order.find(search).then(result => result);

    }catch(err){
        throw err;
    }
}

module.exports.searchUserOrderId = async (orderId, userId) =>{
    try{
        return await Order.findById(orderId).then(result => {
            if (result.orderedById === userId){
                return result
            } else {
                return [];
            }
        })
    }catch(err){
        throw err;
    }
}

module.exports.sendMail = async () => {
    try{
        let transporter = nodemailer.createTransport(
            {
                service: `gmail`,
                auth: {
                    user: ``,
                    password: ``
                }
            }
        );

        let mailOptions = {
            from: ``,
            to: ``,
            subject: `Test Mail`,
            text: `Did you receive this mail?`
        };

        transporter.sendMail(mailOptions, (err, data) => {
            if (err){
                console.log(err);
            } else {
                console.log(successful);
            }
        })
    }catch(err){

    }
}

// module.exports.updateOrderByName = async (userId, userName) => {
//     try{
//         return await Order.updateMany({orderedById: userId}, {orderedBy: userName}).then(result => result? true : false);
//     } catch (err){
//         console.log(err);
//         return err;
//     }
// }
