const Product = require('./../models/Product');
const Order = require('./../models/Order');
const Review = require('./../models/Review');

//------------- Create Product ----------------------
module.exports.createProduct = async (reqBody) => {

    try{
        //console.log(reqBody);
        return await Product.findOne({name: reqBody.name}).then(result =>{
            //no record found
            //console.log(result === null)
            if (result === null){
                //console.log(`if block`)
                let newProduct = new Product({
                    name: reqBody.name,
                    description: reqBody.description,
                    price: reqBody.price,
                    stockCount: reqBody.stockCount,
                    category: reqBody.category,
                    image: reqBody.image
                })
    
                //insert record to database
                return newProduct.save().then((saveResult,err) => {
                    //successful save
                    if(saveResult !== null){
                        return true
                    } else {
                        return false
                    }

                    //print errors if any
                    if (err !== null){
                        return err;
                    }
                })
            //record exists
            } else {
                return { message: `Product already exists`};
            }
        })
    }catch(err){
        return err
    }

}

//-------------- Retrieve All Active Products ----------
module.exports.getAllActiveProducts = async () => {
    try{
        return await Product.find({isActive: true},{image:0}).then(result=>result);
    }catch(err){
        return err
    }
}

//--------------retrieve active products per category-----------
module.exports.getActivePerCategory = async (category) => {
    try{
        return await Product.find({isActive: true, category: category}).then(result=>result);
    }catch(err){
        throw err;
    }
}

//-------------retrieve featured products--------------
module.exports.getFeatured = async () => {
    try{
        return await Product.find({isActive: true, isFeatured: true}).then(result=>result);
    }catch(err){
        throw err;
    }
}

//------------retrieve best sellers----------------------
module.exports.getBestSeller = async (limit) => {
    try{
        return await Product.find({isActive:true}).sort({quantitySold:-1}).limit(limit).then(result => result);
    }catch(err){
        throw err;
    }
}

//------------ Update Product --------------------------------
module.exports.updateProduct = async (productId, reqBody) => {

    try{
        return await Product.findByIdAndUpdate(productId, {$set: reqBody}, {new:true}).then(result=>{
 
            //product does not exist
            if (result !== null){
                //console.log(result)
                let updateOrderProdName = updateOrderPN(result._id, result.name);
                let updateRevProdName = updateReviewPN(result._id, result.name);

                if (updateOrderProdName && updateRevProdName){
                    return true
                } else {
                    return {message: `Succesfully updated product name. Error updating name in orders/reviews.`};
                }
            //successful update
            }else{
                return {message: `Product does not exist.`};
            }
        })
    }catch(err){
        throw err
    }
}

//------------ Retrieve Single Product ------------------------
module.exports.getProduct = async (productId) => {
    try{
        return await Product.findById(productId, {createdOn:0,createdAt:0,updatedAt:0}).then(result => {
            if (result !== null){
                return result
            } else {
                return {message: `Product does not exist.`}
            }
        })
    }catch(err){
        return err
    }
}

//------------- Delete Product (archive/unarchive) -------------
module.exports.changeProductStatus = async (productId) => {
    try{
        let result = await Product.findById(productId, {isActive: 1});

        if ( result == null){
            return {message: `Product does not exist.`};
        }

        return Product.findByIdAndUpdate(productId, {$set: {isActive: !result.isActive}},{new:true} ).then((updateResult, error)=>{

            if (updateResult !== null){
                return true
            }else{
                return error
            }
        })
    }catch(err){
        return err
    }
}

//--------------Retrieve All Products --------------------------
module.exports.getAllProducts = async () => {
    try{
        return await Product.find({},{image:0}).then(result=>result);
    }catch(err){
        return err
    }
}

//--------------Decrease Stocks and Increase QtySold. Called by Create Order function only-------
module.exports.decreaseStocks = async (productId, boughtQty) => {
    try{
        
        let prodStocks = await Product.findById(productId, {stockCount:1, quantitySold:1}).then(result=>result);

        prodStocks.stockCount -= boughtQty;
        prodStocks.quantitySold += boughtQty;

        let update = {
            stockCount: prodStocks.stockCount,
            quantitySold: prodStocks.quantitySold
        }

        if (prodStocks.stockCount === 0){
            update.isActive = false;
        }

        return Product.findByIdAndUpdate(productId, {$set: update}).then((result, error)=> {
            //console.log(result)
            if (result !== null){
                
                return true
            } else {
                console.log(error)
                return error
            }
        })


    }catch(err){
        return err
    }
}

//--------------Search products based on keysearch ----------------
module.exports.productSearch = async (keyword) => {
    try{
        
        let search = 
        {
            $or: [
                { description: {$regex: keyword, $options: "i"} },
                { name: {$regex: keyword, $options: "i"} }
            ]
        }

        return await Product.find(search).then(result => result);
    }catch(err){
        console.log(err);
        return err;
    }
}

//admin search per product using category and keyword
module.exports.productSearchByCategoryAndWord = async (category, keyword) => {
    try{
        
        let search = 
        {
            $and: [
                {category: category},
                {
                    $or: [
                        { description: {$regex: keyword, $options: "i"} },
                        { name: {$regex: keyword, $options: "i"} }
                    ]
                }
            ]
        }

        return await Product.find(search, {image:0}).then(result => result);

    }catch(err){
        console.log(err);
        return err;
    }
}

//admin search product without image
module.exports.productSearchAdmin = async (keyword) => {
    try{
        
        let search = 
        {
            $or: [
                { description: {$regex: keyword, $options: "i"} },
                { name: {$regex: keyword, $options: "i"} }
            ]
        }

        return await Product.find(search, {image:0}).then(result => result);
    }catch(err){
        console.log(err);
        return err;
    }
}

//update product name from order module
function updateOrderPN(pId, pName){
    try{
        let updateQuery = { $set: {"orderItems.$[elem].productName": pName}};
        let filter =     { arrayFilters: [ { "elem.productId": pId } ] }

        return Order.updateMany({},updateQuery,filter).then(result => result ? true : false);
    } catch (err){
        console.log(err);
        throw err;
    }
}

//update reviewer name from review module
function updateReviewPN(pId, pName) {
    try{
        return Review.updateMany({productId: pId}, {productName: pName}).then(result => result ? true : false);
    } catch (err){
        console.log(err);
        throw err;
    }
}

//update isFeatured property
module.exports.updateFeatured = async (productId) => {
    try{
        
        let result = await Product.findById(productId, {isFeatured: 1}).then(result=> result)

        if ( result === null){
            return {message: `Product does not exist.`};
        }

        return Product.findByIdAndUpdate(productId, {$set: {isFeatured: !result.isFeatured}},{new:true} ).then((updateResult, error)=>{

            if (updateResult !== null){
                return true
            }else{
                return error
            }
        })
    }catch(err){
        throw(err);
    }
}
