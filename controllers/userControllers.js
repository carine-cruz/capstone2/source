const User = require('./../models/User');
const CryptoJS = require("crypto-js");
const {createToken} = require('./../auth');
//const { updateOrderByName } = require('./orderControllers');
//const { updateReviewerName } = require('./reviewControllers');
const Order = require('./../models/Order');
const Review = require('./../models/Review');
const Payment = require('./../models/Payment');


//----------User registration-------------
module.exports.userRegistration =  async (reqBody) => {
    try{
        //console.log(reqBody.email);
        return await User.findOne({email: reqBody.email}).then(result =>{
            //no record found
            
            if (result === null){
                //console.log(`if block`)
                let newUser = new User({
                    email: reqBody.email,
                    password: CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString(),
                    firstName: reqBody.firstName,
                    lastName: reqBody.lastName,
                    mobileNo: reqBody.mobileNo
                })
    
                //insert record to database
                return newUser.save().then((saveResult,err) => {
                    //successful save
                    if(saveResult !== null){
                        return true
                    } else {
                        return false
                    }

                    //print errors if any
                    if (err !== null){
                        return err;
                    }
                })
            //record exists
            } else {
                return { message: `Email address already registered.`};
            }
        })
    }catch(err){
        console.log(err)
    }

}

//---------email exists--------------------
module.exports.emailExists = async (reqBody) => {
  try{
    return await User.findOne({email: reqBody.email}).then(result =>{
        if (result === null){
            return false;
        }else{
            return true;
        }
    })
  } catch(err) {
      throw err;
  }
}

//----------User login--------------------
module.exports.login = async (reqBody) => {
    
    try{
        return await User.findOne({email: reqBody.email}, {email:1, password:1, isAdmin:1, isActive: 1}).then((result, error) => {

            //no result found, user not yet registered
            if (result == null){
                return {message: `User not yet registered.`}
            //user found. decrypt password and check if matches with database
            } else {
                //account deactivated
                if (result.isActive){
                    let decodedPw = CryptoJS.AES.decrypt(result.password, process.env.SECRET_PASS).toString(CryptoJS.enc.Utf8)

                    if (reqBody.password === decodedPw){
                        return { token: createToken(result) }
                    } else {
                        return {message: `Password and email does not match.`}
                    }
                //account deactivated
                }else{
                    return {message: `Unable to login. Account is inactive.`}
                }
            }
        })
    }catch(err){
        return err
    }
}

//----------Change User to Admin ---------
module.exports.changeAdminStatus = async(userId) =>{
    try{
        let result = await User.findById(userId, {isAdmin: 1});

        return User.findByIdAndUpdate(userId, {$set: {isAdmin: !result.isAdmin}},{new:true} ).then((updateResult, error)=>{

            if (updateResult !== null){
                return true
            }else{
                return error
            }
        })
    }catch(err){
        return err
    }
}

//accessed by orderControllers.createOrder only
//----------Retrieve Name ----------------
module.exports.getName = async (userId) => {
    try{
        
        let  dbName = await User.findById(userId, {firstName:1, lastName:1, _id: 0}).then(result=>result)
        return {
            firstName: dbName.firstName,
            lastName: dbName.lastName,
            fullName: `${dbName.firstName} ${dbName.lastName}`
        };
    }catch(err){
        return err;
    }
}

//----------Get Own Profile (using token)----------
module.exports.getProfile = async (userId) => {
    try{
        return await User.findById(userId, {isActive:0, createdAt:0, updatedAt:0}).then( (result, err) =>{
            if(result){
                return result
            } else {
                if (result == null){
                    return {message: `User does not exist`}
                } else {
                    return err
                }
            }
        })
    }catch(err){

    }
}

//----------Get Profiles of All Users --------------
module.exports.getAllUsers = async () => {
    try{
        return await User.find({},{password:0}).then(result => result);
    }catch(err){
        return err
    }
}

//----------Get Profile (for Admin, using params)--------
module.exports.getUser = async (userId) => {
    try{
        return await User.findById(userId).then( (result, err) =>{
            if(result){
                return result
            } else {
                if (result == null){
                    return {message: `User does not exist`}
                } else {
                    return err
                }
            }
        })
    }catch(err){
        return err;
    }
}

//----------Update User Details ------------------
module.exports.updateProfile = async (userId, reqBody) => {
    try{
        //if body contains password, encrypt password
        if(reqBody.hasOwnProperty('password')){
            reqBody.password = CryptoJS.AES.encrypt(reqBody.password, process.env.SECRET_PASS).toString();
        }
        
        return await User.findByIdAndUpdate(userId, {$set: reqBody},{new:true}).then((result, err) => {
            //console.log(result)
            if(result){
                let fullName = `${result.firstName} ${result.lastName}`;
                let updateOrders = updateOrderByName(userId, fullName);
                let updateReviews = updateReviewerName(userId, fullName);
                let updatePayments = updatePaymentName(userId, fullName);
                
                if (updateOrders && updateReviews && updatePayments){
                    return true
                } else {
                    return {message: `Succesfully updated profile. Error updating name in orders/reviews/payments.`};
                }
                
            } else {
                if (result == null){
                    return {message: `User does not exist`}
                } else {
                    return err
                }
            }
        })
    }catch(err){
        return err;
    }
}

//----------Update Own Password (token)------------------
module.exports.updatePassword = async (userId, pw) => {

    newPassword = CryptoJS.AES.encrypt(pw, process.env.SECRET_PASS).toString();

    return await User.findByIdAndUpdate(userId, {$set: {password: newPassword}}).then((result, error) => {
        
        //successful password update
        if(result){
            return true
        //error
        } else {
            //return false;
            return error
        }
    })
}

//----------Delete (Archive) Users ----------------------
module.exports.updateAccess = async (userId) => {
    try{
        let result = await User.findById(userId, {isActive: 1});

        return User.findByIdAndUpdate(userId, {$set: {isActive: !result.isActive}},{new:true} ).then((updateResult, error)=>{

            if (updateResult !== null){
                return true
            }else{
                return error
            }
        })
    }catch(err){
        return err
    }
}

//accessed by orderControllers.createOrder() only -- REMOVED
module.exports.addOrder = async (userId, orderId) => {
    // try{

    //     //find user, push order to array and update
    //     return await User.findById(userId).then(result => {
            
    //         if (result !== null){
                
    //             let orderExists = false;
    //             //loop through array to check if order already exists before adding
    //             for (let userOrder of result.orders){
                    
    //                 if (userOrder.orderId == orderId){
    //                     orderExists = true;
    //                     break;
    //                 }
    //             }
    //             //if order does not exist, push to array and save
    //             if (orderExists == false){
    //                 result.orders.push({orderId: orderId});
    //                 return result.save().then(orderResult => orderResult ? true : false)
                    
    //             } else {
    //                 return {message: `Cannot duplicate same order for this user.`}
    //             }

    //         }else{
    //             return {message: `User not found. Order not added.`};
    //         }
    //     })
    // }catch(err){
    //     return err
    // }
}

//--------search for user (name) --------------------
module.exports.searchUser = async (keyword) => {
    try{
        
        let search = 
        {
            $or: [
                { firstName: {$regex: keyword, $options: "i"} },
                { lastName: {$regex: keyword, $options: "i"} }
            ]
        }

        return await User.find(search).then(result => result);
    }catch(err){
        console.log(err);
        return err;
    }
}

function updateOrderByName (userId, userName) {
    try{
        return Order.updateMany({orderedById: userId}, {orderedBy: userName}).then(result => result? true : false);
    } catch (err){
        console.log(err);
        return err;
    }
}

function updateReviewerName (userId, userName) {
    try{
        return Review.updateMany({reviewerId: userId}, {reviewerName: userName}).then(result => result? true : false);
    } catch (err){
        console.log(err);
        return err;
    }
}

function updatePaymentName (userId, userName) {
    try{
        return Payment.updateMany({customerId: userId},{customerName: userName}).then(result => result? true : false);
    } catch (err) {
        throw err;
    }
}

