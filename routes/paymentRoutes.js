const express = require('express');
const router = express.Router();
const {verify, verifyAdmin, verifyNonAdmin, decode} = require('./../auth');
const { 
    pay, 
    getAllPayments, 
    searchPayment, 
    getUserPayments, 
    getPayment, 
    searchRefNo, 
    searchMyRef,
    searchCustomer,
    searchCustomerOrder 
} = require('./../controllers/paymentControllers')

//make payment
router.post('/pay/:orderId', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await pay(userId, req.params.orderId, req.body).then(result => res.send(result));
    } catch(err){
        res.status(500).json(err);
    }
})

//retrieve all payments --admin
router.get('/all-payments', verifyAdmin, async (req, res) => {
    try{
        await getAllPayments().then(result=>res.send(result));
    }catch (err){
        res.status(500).json(err);
    }
})

//retrieve user own payments
router.get('/my-payments', verifyNonAdmin, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await getUserPayments(userId).then(result=>res.send(result));
    }catch (err){
        res.status(500).json(err);
    }
})

//search payments
router.get('/search-payments/:id', verifyAdmin, async (req, res) => {
    try{
        await searchPayment(req.params.id).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve one payment details by ID
router.get('/payment-details/:paymentId', verify, async (req, res) => {
    try{
        await getPayment(req.params.paymentId).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//admin search all reference no
router.get('/search-ref/:refno', verifyAdmin, async (req, res) => {
    try{
        await searchRefNo(req.params.refno).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//user search own reference no
router.get('/search-my-ref/:refno', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await searchMyRef(req.params.refno, userId).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//admin search customer name
router.get('/search-customer/:keyword', verifyAdmin, async (req, res) => {
    try{
        await searchCustomer(req.params.keyword).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//user search own order
router.get('/search-my-order/:keyword', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await searchCustomerOrder(req.params.keyword, userId).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

module.exports = router;