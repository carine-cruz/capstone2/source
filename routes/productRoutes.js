const express = require('express');
const router = express.Router();
const {verify, verifyAdmin, decode, verifyNonAdmin} = require('./../auth');
const {
    createProduct 
    ,getAllActiveProducts 
    ,updateProduct 
    ,getProduct
    ,changeProductStatus
    ,getAllProducts
    ,productSearch
    ,updateFeatured
    ,getActivePerCategory
    ,getFeatured
    ,getBestSeller
    ,productSearchByCategoryAndWord
    ,productSearchAdmin
    //,updateOrderPN
    //,updateReviewPN
} = require('./../controllers/productControllers');

//Retrieve all active products
router.get('/', async (req, res) => {
    try{
        await getAllActiveProducts().then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve products per category
router.get('/category/:category', async (req, res) => {
    try{
        await getActivePerCategory(req.params.category).then(result => res.send(result));
    } catch(err){
        res.status(500).json(err);
    }
})

//retrieve featured products
router.get('/featured', async (req, res) => {
    try{
        await getFeatured().then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve best seller products
router.get('/best-seller/:count', async (req, res) => {
    try{
        await getBestSeller(req.params.count).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Update product information (admin only)
router.put('/update/:productId', verifyAdmin, async (req, res) => {
    try{
        return await updateProduct(req.params.productId, req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Create product (admin only)
router.post('/new-product', verifyAdmin, async (req, res) => {
    try{
        await createProduct(req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//retrieve single product
router.get('/product/:productId', async (req, res) => {
    try{
        await getProduct(req.params.productId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Archive/unarchive product (admin only)
router.patch('/active-status/:productId', verifyAdmin, async (req, res) => {
    try{
        await changeProductStatus(req.params.productId).then(result => res.send(result));
    }catch(err){
        return err
    }
})

//FEATURES
//Retrieve all products -admin
router.get('/all-products', verifyAdmin, async (req, res) => {
    try{
        await getAllProducts().then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//product search
router.get('/search-products/:keyword', async (req, res) => {
    
    try{
        await productSearch(req.params.keyword).then(result => res.send(result));
    }catch(err) {
        res.status(500).json(err);
    }
})

//change isFeatured status
router.patch('/update-featured/:productId', verifyAdmin, async (req, res) => {

    try{
        
        await updateFeatured(req.params.productId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//admin search using keyword and category
router.get('/admin-search/:category/keyword/:keyword', verifyAdmin, async (req, res) => {
    try{
        await productSearchByCategoryAndWord(req.params.category, req.params.keyword).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//admin search all products without image result
router.get('/admin-search-all/:keyword', verifyAdmin, async (req, res) => {
    try{
        await productSearchAdmin(req.params.keyword).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

module.exports = router;