const express = require('express');
const router = express.Router();

const {verify, verifyAdmin, decode} = require('./../auth');

const { 
    userRegistration 
    ,login 
    ,changeAdminStatus 
    ,getName 
    ,getProfile
    ,getAllUsers
    ,getUser
    ,updateProfile
    ,updatePassword
    ,updateAccess
    ,searchUser
    ,emailExists
    //,addOrder
} = require('./../controllers/userControllers');

//User registration
router.post('/user-registration', async (req, res) => {
    try{
        await userRegistration(req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//User authentication/login
router.post('/login', async (req, res) => {
    try{
        await login(req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//Set user as admin, or admin to user (admin only)
router.patch('/change-user-status/:userId', verifyAdmin, async (req, res) => {
    try{
        await changeAdminStatus(req.params.userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err)
    }
})

//exposed to other modules to get full name
router.get('/get-name/:userId', verify, async (req, res) =>{
    console.log(req.params.userId);
    try{
        await getName(req.params.userId).then(result=>res.send(result));
    }catch(err){
        return err;
    }
})

//get own profile
router.get('/my-profile', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await getProfile(userId).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err)
    }
})

//get all profile (admin)
router.get('/all-users', verifyAdmin, async (req, res) => {
    try{
        await getAllUsers().then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

// get one user profile
router.get('/profile/:userId', verify, async (req, res) => {
    try{
        await getUser(req.params.userId).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//update profile
router.put('/update/:userId', verify, async (req, res) => {
    try{
        //let userId = decode(req.headers.authorization).id;
        await updateProfile(req.params.userId, req.body).then(result=>res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

//update own password (using token)
router.patch('/update-password', verify, async (req, res) => {
    try{
        let userId = decode(req.headers.authorization).id;
        await updatePassword(userId, req.body.password).then(result=>res.send(result));
    }catch(err){
        return err;
    }
})

//update password
router.patch('/update-user-password/:userId', verify, async (req, res) => {
    try{
        await updatePassword(req.params.userId, req.body.password).then(result=>res.send(result));
    }catch(err){
        return err;
    }
})

//delete profile (admin) --change active status
router.patch('/user-access/:userId', verifyAdmin, async (req, res) => {
    try{
        await updateAccess(req.params.userId).then(result=>res.send(result));
    }catch(err){
        return err
    }
})

//add order (temp)
// router.put('/add-order/:userId/order/:orderId', verify, async (req, res) => {
//     try{
//         await addOrder(req.params.userId, req.params.orderId).then(result => res.send(result));
//     }catch(err){
//         res.status(500).json(err);
//     }
// })

//search user Id (last name)
router.get('/search-user/:keyword', verifyAdmin, async (req, res) => {
    try{
        await searchUser(req.params.keyword).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

router.post('/email-exists', async (req, res) => {
    try{
        await emailExists(req.body).then(result => res.send(result));
    }catch(err){
        res.status(500).json(err);
    }
})

module.exports = router;