//import required libraries
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');
const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3008;
const app = express();

//setup cors
app.use(bodyParser.json({limit:'300mb'}));
app.use(bodyParser.urlencoded({ limit:'300mb', extended: true }));

const corsOptions = {
	origin: ['http://localhost:3000','https://superb-sorbet-686cbe.netlify.app', 'https://the-bakery-ph.netlify.app', 'https://carinecruz.github.io/ecommerce'],
	optionsSuccessStatus: 200
};

app.use(cors(corsOptions));
app.use(express.json());
app.use(express.urlencoded({extended: true}));


//connect our routes module
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const reviewRoutes = require('./routes/reviewRoutes');
const paymentRoutes = require('./routes/paymentRoutes');

//middleware to handle JSON payloads
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//CORS -prevents blocking of requests from frontend(client) esp different domains
app.use(cors());

//connect Database to server
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser:true, useUnifiedTopology:true});

//test DB connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => console.log('Connected to Database'));

//Routes
app.use('/api/users', userRoutes);
app.use('/api/products', productRoutes);
app.use('/api/orders', orderRoutes);
app.use('/api/reviews', reviewRoutes);
app.use('/api/payment', paymentRoutes);

app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))