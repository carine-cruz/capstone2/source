const jwt = require('jsonwebtoken');

//sign
module.exports.createToken = (data) => {
    
    //payload (user data)
    let userData = {
        id: data._id,
        email: data.email,
        isAdmin: data.isAdmin
    }

    //creates the token
    try{
        return jwt.sign(userData, process.env.SECRET_PASS);
    }catch(err){
        return err
    }

}

//verify
module.exports.verify = (req, res, next) => {
    //third parameter is middleware
    try{
        const requestToken = req.headers.authorization;
        //console.log(requestToken);
    
        if(typeof requestToken == "undefined"){
            //typeof returns data type in string format
            res.status(401).send(`Invalid login attempt. Token not issued.`);
        } else {
            const token = requestToken.slice(7);
            //console.log(token);
    
            if(typeof token !== "undefined"){
    
                //start actual verification
                jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
                    if (err){
                        return res.send({message: `Authentication failed!`})
                    } else {
                        next();
                        //go to next function
                    }
                })
            }
        }
    }catch(err){
        return err
    }
}

//decode
module.exports.decode = (bearerToken) => {

    try{
        // const token = bearerToken.slice(7, bearerToken.length);
    
        // return jwt.decode(token);
         //no token
         if ((typeof bearerToken) == "undefined"){
            //return res.status(401).send(`Invalid login attempt. Token not issued.`);
            return false
        //with token
        } else {
            //extract and decode new token
            let token = jwt.decode(bearerToken.slice(7));
            return token;
        }
    }catch(err){
        return err
    }
}

//isAdmin access
module.exports.verifyAdmin = (req, res, next) => {
    try{
        const requestToken = req.headers.authorization
        // console.log(requestToken)
        
        if(typeof requestToken == "undefined"){
            res.status(401).send(`Invalid login attempt. Token not issued.`)
    
        }else{
            const token = requestToken.slice(7);
            
                if(jwt.decode(token).isAdmin == true){
                    
                    return jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
                        if(err){
                            return res.send({message: `Authentication failed!`})
    
                        } else{
                            next()
                        }
                    })
                } else {
                    res.status(403).send(`Unauthorized access.`)
                }
        }
    }catch(err){
        return err;
        //console.log(err)
    }
}

//users only
module.exports.verifyNonAdmin = (req, res, next) => {
    try{

        // let token = this.decode(req.headers.authorization);
        
        // //no token
        // if (token === false){
        //     return res.status(401).send(`Invalid login attempt. Token not issued.`);
        // //with token
        // } else{
        //     //check if non-admin
        //     //console.log(token.isAdmin === false)
        //     if (token.isAdmin === false){
        //         console.log(`start verify`)
        //         let nToken = removeTokenHeader(req.headers.authorization);
        //         console.log(nToken);
                
        //     } else {
        //         console.log(token)
        //         return res.status(401).send(`Administrator not allowed to perform this action.`);
        //     }
        // }
            const requestToken = req.headers.authorization
            
            if(typeof requestToken == "undefined"){
                res.status(401).send(`Invalid login attempt. Token not issued.`)
        
            }else{
                const token = requestToken.slice(7);
                
                    if(jwt.decode(token).isAdmin == false){
                        
                        return jwt.verify(token, process.env.SECRET_PASS, (err, data) => {
                            if(err){
                                return res.send({message: `Authentication failed!`})
        
                            } else{
                                next()
                            }
                        })
                    } else {
                        res.status(403).send(`Administrator not allowed to perform this action.`)
                    }
            }

    }catch(err){
        return err
    }
}