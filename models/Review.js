const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
    
    productId: {
        type: String,
        required: [true, `Product Id required.`]
    },
    productName: {
        type: String,
        required: [true, `Product name is required.`]
    },
    reviewerId: {
        type: String,
        required: [true, `Reviewer ID is required.`]
    },
    reviewerName: {
        type: String,
        required: [true, `Reviewer name is required.`]
    },
    comments: {
        type: String
    },
    rating: {
        type: Number,
        required: [true, `Rating is required.`]
    },
    reviewDate: {
        type: Date,
        default: new Date()
    }
},{timestamps: true});

module.exports = mongoose.model(`Review`, reviewSchema);