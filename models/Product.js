const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, `Product Name is required.`]
    },
    description: {
        type: String
    },
    price: {
        type: Number,
        required: [true, `Product price is required.`]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    image: [{
        // data: Buffer,
        // contentType: String
        type: String
    }],
    // reviews : [
    //     {
    //         reviewerId: {
    //             type: String,
    //             required: [true, `Reviewer ID is required.`]
    //         },
    //         reviewerName: {
    //             type: String,
    //             required: [true, `Reviewer name is required.`]
    //         },
    //         comments: {
    //             type: String
    //         },
    //         rating: {
    //             type: Number,
    //             required: [true, `Reviewer name is required.`],
    //             min: 0,
    //             max: 10
    //         },
    //         reviewDate: {
    //             type: Date,
    //             default: new Date()
    //         }
    //     }
    // ],
    quantitySold: {
        type: Number,
        default: 0,
        min: 0
    },
    stockCount: {
        type: Number,
        required: [true, `Stock count is required.`]
    },
    category: {
        type: String,
        required: [true, `Category is required.`]
    },
    isFeatured: {
        type: Boolean,
        default: false
    }
}, {timestamps:true})

module.exports = mongoose.model(`Product`, productSchema);
