const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: [true, `Email address is required`],
        unique: true
    },
    password: {
        type: String,
        required: [true, `Password is required`]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    firstName: {
        type: String,
        required: [true, `First name is required.`]
    },
    lastName: {
         type: String,
        required: [true, `Last name is required.`]
    },
    mobileNo: {
        type: String,
        required: [true, `Mobile number is required.`]
    },
    isActive:{
        type: Boolean,
        default: true
    },
    cart: [{
        productId: {
            type: String,
            required: [true, `Product ID is required.`]
        },
        productName: {
            type: String,
            required: [true, `Product name is required.`]
        },
        productQty: {
            type: Number,
            required: [true, `Ordered product quantity is required.`]
        },
        productPrice: {
            type: Number,
            required: [true, `Product price is required.`]
        },
        subTotal: {
            type: Number,
            required: [true, `Order subtotal is required.`]
        }
    }]
    // ,orders: [
    //     {
    //         orderId: {
    //             type: String,
    //             required: [true, `Order ID is required.`]
    //         },
    //         orderStatus: {
    //             type: String,
    //             default: "On-going",
    //             required: [true, `Order status is required.`]
    //         },
    //         orderDate: {
    //             type: Date,
    //             default: new Date()
    //         },
    //         orderFulfilled: {
    //             type: Date
    //         }
    //     }
    // ]
}, {timestamps: true})

module.exports = mongoose.model(`User`, userSchema);